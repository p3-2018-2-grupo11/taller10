#include <stdio.h>
#include <png.h>
#include <stdlib.h>
#include <stdio.h>
#include <png.h>
#include  <string.h>
#include  <unistd.h>
#include  <pthread.h>
#include <time.h>
png_bytep *res;
int width, height;
png_bytep *pixeles;
int nh;
double tiempo(){
  struct timespec tsp;

  clock_gettime(CLOCK_REALTIME, &tsp);

  double secs = (double)tsp.tv_sec;
  double nano = (double)tsp.tv_nsec / 1000000000.0;

  return secs + nano;
}

//Retorna los pixeles de la imagen, y los datos relacionados en los argumentos: ancho, alto, tipo de color (normalmente RGBA) y bits por pixel (usualemente 8 bits)
png_bytep * abrir_archivo_png(char *filename, int *width, int *height, png_byte *color_type, png_byte *bit_depth) {
  FILE *fp = fopen(filename, "rb");

  png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if(!png) abort();

  png_infop info = png_create_info_struct(png);
  if(!info) abort();

  if(setjmp(png_jmpbuf(png))) abort();

  png_init_io(png, fp);

  png_read_info(png, info);

  //resolucion y color de la image. Usaremos espacio de color RGBA
  *width      = png_get_image_width(png, info);
  *height     = png_get_image_height(png, info);
  *color_type = png_get_color_type(png, info);
  *bit_depth  = png_get_bit_depth(png, info);

  // Read any color_type into 8bit depth, RGBA format.
  // See http://www.libpng.org/pub/png/libpng-manual.txt

  if(*bit_depth == 16)
    png_set_strip_16(png);

  if(*color_type == PNG_COLOR_TYPE_PALETTE)
    png_set_palette_to_rgb(png);

  // PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
  if(*color_type == PNG_COLOR_TYPE_GRAY && *bit_depth < 8)
    png_set_expand_gray_1_2_4_to_8(png);

  if(png_get_valid(png, info, PNG_INFO_tRNS))
    png_set_tRNS_to_alpha(png);

  // These color_type don't have an alpha channel then fill it with 0xff.
  if(*color_type == PNG_COLOR_TYPE_RGB ||
     *color_type == PNG_COLOR_TYPE_GRAY ||
     *color_type == PNG_COLOR_TYPE_PALETTE)
    png_set_filler(png, 0xFF, PNG_FILLER_AFTER);

  if(*color_type == PNG_COLOR_TYPE_GRAY ||
     *color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
    png_set_gray_to_rgb(png);

  png_bytep *row_pointers;
  png_read_update_info(png, info);
  row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * (*height));
  if(row_pointers == NULL){
      printf("Error al obtener memoria de la imagen\n");
      exit(-1);
  }
  for(int y = 0; y < *height; y++) {
    row_pointers[y] = (png_byte*)malloc(png_get_rowbytes(png,info));
  }

  png_read_image(png, row_pointers);

  fclose(fp);
  return row_pointers;
}

//Usaremos bit depth 8
//Color type PNG_COLOR_TYPE_GRAY_ALPHA
void guardar_imagen_png(char *filename, int width, int height, png_byte color_type, png_byte bit_depth, png_bytep *res1) {
  //int y;

  FILE *fp = fopen(filename, "wb");
  if(!fp) abort();

  png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!png) abort();

  png_infop info = png_create_info_struct(png);
  if (!info) abort();

  if (setjmp(png_jmpbuf(png))) abort();

  png_init_io(png, fp);

  // Salida es escala de grises
  png_set_IHDR(
    png,
    info,
    width, height,
    bit_depth,
    color_type,
    PNG_INTERLACE_NONE,
    PNG_COMPRESSION_TYPE_DEFAULT,
    PNG_FILTER_TYPE_DEFAULT
  );
  png_write_info(png, info);

  // To remove the alpha channel for PNG_COLOR_TYPE_RGB format,
  // Use png_set_filler().
  //png_set_filler(png, 0, PNG_FILLER_AFTER);
  png_write_image(png, res1);	//row_pointers
  png_write_end(png, NULL);
  for(int y = 0; y < height; y++) {
    free(res1[y]);		//row_pointers
  }
  free(res1);			//row_pointers

  fclose(fp);
}


void *procesar_imagen(void *arg){


	int *hilo;
	hilo=(int *)arg;
	int sum=*hilo;

	//gettimeofday(&t1, NULL);
	//unsigned  int  ut1 = t1.tv_sec *1000000+ t1.tv_usec ;
	//unsigned  int  ut0 = t0.tv_sec *1000000+ t0.tv_usec ;
	//media += (ut1 -ut0 );
	for(int y = sum; y < height; y=y+nh) {
    png_bytep row = pixeles[y];
  
    //espacio para la los pixeles de dicha fila
    res[y] = malloc(sizeof(png_bytep)*width*2);			//greyscale con alpha, 2 bytes por pixel
    
    if(res[y] == NULL){
      printf("No se pudo reservar espacio para la imagen procesada");
      exit(-1);
    }
//printf("El hilo: %d con la fila %d  \n",sum,y );
    
    	for(int x = 0; x < width; x++) {
      png_bytep px = &(row[x * 4]);

      //Convertimos a escala de grises
      float a = .299f * px[0] + .587f * px[1] + .114f * px[2];
      res[y][2*x] = a;
      res[y][2*x + 1] = px[3]; //transparencia... dejamos el campo de transparencia igual.

    }

}
}

//Retorna el arreglo de pixeles
void *procesar_archivo_png(int width, int height, png_bytep *row_pointers,int i) {
  //filas de nueva imagen
  res = (png_bytep*)malloc(sizeof(png_bytep *) * height);
  if(res == NULL){
      printf("No se pudo reservar espacio para la imagen procesada");
      exit(-1);
  }

  int *index = NULL;
  pthread_t *ptr;
  index = calloc (nh, sizeof (int));
  for(int i = 0; i < nh; i++)
        {
                index[i] = i;
        }
  ptr = malloc(sizeof(pthread_t)*nh);


  for (int k =0; k<nh;k++){
  
  pthread_create(&ptr[k], NULL , procesar_imagen ,(void*)&index[k]);

}
for(int i = 0; i < nh; i++){
               pthread_join(ptr[i], NULL); 
              }

}

int main(int argc, char *argv[]) {
  if(argc != 5) abort();
  nh=atoi(argv[4]);
  png_byte color_type; 
  png_byte bit_depth;
  pixeles = abrir_archivo_png(argv[1],&width, &height, &color_type, &bit_depth);
  double ini = tiempo();
  procesar_archivo_png(width,height,pixeles,atoi(argv[4]));
  double fin = tiempo();
  double delta = fin - ini;
  printf("%f\n",delta );
  guardar_imagen_png(argv[2], width, height, PNG_COLOR_TYPE_GRAY_ALPHA, bit_depth, res);
  


  return 0;
}